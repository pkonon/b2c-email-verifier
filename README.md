## Library allows to pass b2c email verification

### To get random email
```
EmailVerifier.generateEmail().then(email => {
    console.log(email);
});
```

### To fetch verification
```
EmailVerifier.getCode('randome@example.com', { timeout: 10000 }).then(resp => {
    console.log(`Success: ${resp}`);
}).catch(err => {
    console.error(err);
});
```
