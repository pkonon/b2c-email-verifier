var mail = require('temp-mail');
var jsdom = require('jsdom');

module.exports = class EmailVerifier {
    static generateEmail() {
        return mail.generateEmail();
    }

    static getCode(email, { timeout = 0 } = {}) {
        return EmailVerifier._getEmailText(email, timeout).then(emailText => {
            return EmailVerifier._findCode(emailText);
        });
    }

    static _getEmailText(emailAddress, timeout) {
        var endDate = Date.now() + timeout;
        return mail.getInbox(emailAddress).then(inbox => {
            var microsoftLetter = inbox.find(item => {
                return item.mail_from.indexOf('microsoftonline.com') !== -1;
            });
            if (microsoftLetter) {
                return microsoftLetter.mail_text_only;
            }
        }).catch(err => {
            if (endDate <= Date.now()) {
                throw err;
            }
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve(EmailVerifier._getEmailText(emailAddress, endDate - Date.now()));
                }, 1000);
            });
        });
    }

    static _findCode(text) {
        return new Promise((resolve, reject) => {
            jsdom.env(
                text, [],
                function(errors, window) {
                    try {
                        var code = window.document.querySelector(
                                '[id$=_UserVerificationEmailBodySentence2]').innerHTML
                            .match(/\d+/)[0];
                        resolve(code);
                    } catch (e) {
                        reject(e);
                    }
                    if (window) {
                        window.close();
                    }
                }
            );
        });
    }
}
